using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionChecks : MonoBehaviour
{
    public bool checkHighAndLow;    
    public float height;
    public bool onGround;
    public bool leftBlock;
    public bool rightBlock;
    public Vector2 rayOffset;
    public float rayLengthGround = 0.1f;
    public float rayLengthSides = 0.1f;

    public LayerMask layerMaskGround;
    public LayerMask layerMaskSide;


    // Update is called once per frame
    public void CollisionUpdate()
    {
        GroundCheck();
        LeftCheck();
        RightCheck();
    }

    void GroundCheck() 
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, rayLengthGround, layerMaskGround);

        if (hit.collider != null)
        {
            onGround = true;
        }
        else 
        {
            onGround = false;
        }
    }

    void LeftCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(-rayOffset.x, rayOffset.y, 0), -Vector2.right, rayLengthSides, layerMaskSide);

        if (hit.collider != null)
        {
            leftBlock = true;
        }
        else if(checkHighAndLow)
        {
            LeftCheckHigh();
        }
        else
        {
            leftBlock = false;
        }
    }
    void LeftCheckHigh()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(-rayOffset.x, rayOffset.y + height, 0), -Vector2.right, rayLengthSides, layerMaskSide);

        if (hit.collider != null)
        {
            leftBlock = true;
        }
        else
        {
            leftBlock = false;
        }
    }

    void RightCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(rayOffset.x, rayOffset.y, 0), Vector2.right, rayLengthSides, layerMaskSide);

        if (hit.collider != null)
        {
            rightBlock = true;
        }
        else if (checkHighAndLow)
        {
            RightCheckHigh();
        }
        else
        {
            rightBlock = false;
        }
    }

    void RightCheckHigh()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(rayOffset.x, rayOffset.y + height, 0), Vector2.right, rayLengthSides, layerMaskSide);

        if (hit.collider != null)
        {
            rightBlock = true;
        }
        else
        {
            rightBlock = false;
        }
    }
}
