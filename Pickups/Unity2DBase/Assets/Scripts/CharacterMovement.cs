using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float maxSpeed = 4f;
    [Range(0.0f, 1.0f)]
    public float acceleration = 1f;
    [Range(0.0f, 1.0f)]
    public float deceleration = 1f;

    Vector2 _desiredVelocity;
    Vector2 _velocity;

    PlayerController _cont;
    Rigidbody2D _rb;
    CollisionChecks _col;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _cont = GetComponent <PlayerController>();
        _col = GetComponent<CollisionChecks>();
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        CheckBlock();
        SetVelocity();
        _rb.velocity = _velocity;
    }

    void CheckBlock() 
    {
        if (_col.leftBlock) 
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, 0, 1);
            _velocity.x = Mathf.Clamp(_velocity.x, 0, maxSpeed);
        }
        if (_col.rightBlock)
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, -1, 0);
            _velocity.x = Mathf.Clamp(_velocity.x, -maxSpeed, 0);
        }
    }

    void SetVelocity() 
    {
        _velocity = new Vector2(_velocity.x, _rb.velocity.y);

        if (_cont.inputX != 0)
        {
            _desiredVelocity = new Vector2(_cont.inputX, 0) * (maxSpeed );
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, acceleration * Time.deltaTime * 240);
        }
        else 
        {
            _desiredVelocity = Vector2.zero;
            _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, deceleration * Time.deltaTime * 240);
        }
    }
}
