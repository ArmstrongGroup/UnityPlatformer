using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool inTrigger = false;
    public string effectedTag;

    public delegate void TriggerEntered();
    public TriggerEntered myTrigEntered;

    public delegate void TriggerExited();
    public TriggerEntered myTrigExited;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == effectedTag) 
        {
            inTrigger = true;

            if (myTrigEntered != null)
            {
                myTrigEntered();
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == effectedTag)
        {
            inTrigger = false;

            if (myTrigExited != null)
            {
                myTrigExited();
            }
        }
    }
}
