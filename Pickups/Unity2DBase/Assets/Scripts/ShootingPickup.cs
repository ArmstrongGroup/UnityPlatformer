using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingPickup : MonoBehaviour
{
    TriggerScript _trigger;
    PlayerShooting _pShooting;
    SpriteRenderer _sRend; 

    // Start is called before the first frame update
    void Awake()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _pShooting = FindObjectOfType<PlayerShooting>();
        _sRend = GetComponent<SpriteRenderer>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _trigger.myTrigEntered += PickedUp;
    }

    void PickedUp() 
    {
        _pShooting.hasGun = true;
        _trigger.enabled = false;
        _sRend.enabled = false;
    }
}
