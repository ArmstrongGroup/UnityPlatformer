using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    public bool addPlatformVelocityForJumps;
    [Range (0.0f, 1.0f)]
    public float amountVelocityAdded = 0.75f;

    PlayerStateController _pStateCont;
    Rigidbody2D _rb;
    Rigidbody2D _platRB;
    Vector2 _addVelocity;
    bool _updateAddVelocity;

    // Start is called before the first frame update
    void Start()
    {
        _pStateCont = GetComponent<PlayerStateController>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Hazard"))
        {
            KillPlayer();
        }
        if (collision.collider.CompareTag("Enemy"))
        {
            if (collision.gameObject.transform.position.y > (transform.position.y - 0.4f))
            {
                KillPlayer();
            }
            else
            {
                collision.gameObject.BroadcastMessage("Die");
                _rb.velocity = Vector2.zero;
                _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);
            }
        }
        if (collision.collider.CompareTag("MovingPlatform"))
        {
            //transform.SetParent(collision.collider.transform);
            _platRB = collision.collider.GetComponent<Rigidbody2D>();
            _updateAddVelocity = true;
            //_rb.velocity = _rb.velocity + _platRB.velocity;
        }
        else if (collision.collider.tag != "MovingPlatform")
        {
            //_platRB = null;
            _addVelocity = Vector2.zero;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("MovingPlatform"))
        {
            _updateAddVelocity = false;

            if (!addPlatformVelocityForJumps)
            {
                _addVelocity = Vector2.zero;
            }
            else 
            {
                _addVelocity.x = _addVelocity.x * 0.75f;
            }
        }
    }

    public  void LateUpdate()
    {
        if (_updateAddVelocity)
        {
            _addVelocity.x = _platRB.velocity.x;
        }
        _rb.velocity = _rb.velocity + _addVelocity;
    }

    void KillPlayer() 
    {
        _pStateCont.PlayerDeath();
        //Debug.Log("Player Should Die");
    }
}
