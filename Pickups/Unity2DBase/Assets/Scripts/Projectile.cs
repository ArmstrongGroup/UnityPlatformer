using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _rb.AddForce(transform.right * 10, ForceMode2D.Impulse);
        Physics2D.IgnoreLayerCollision(3, 9);
        Invoke("End", 10);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }

    void End() 
    {
        Destroy(gameObject);
    }
}
