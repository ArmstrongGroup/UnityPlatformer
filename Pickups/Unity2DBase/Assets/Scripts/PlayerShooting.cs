using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooting : MonoBehaviour
{
    public GameObject bullet;
    public bool hasGun;

    PlayerController _pController;
    float _shootDir = 1;
    Vector3 _projectileRot = Vector3.zero;

    // Start is called before the first frame update
    void Start()
    {
        _pController = GetComponent<PlayerController>();
    }

    // Update is called once per frame
    public void ShootUpdate()
    {
        if (hasGun)
        {
            UpdateShootDir();
            Shoot();
        }
    }

    private void UpdateShootDir()
    {
        if (_pController.inputX != 0) 
        {
            if (_pController.inputX > 0)
            {
                _shootDir = 1;
                _projectileRot = Vector3.zero;
            }
            else 
            {
                _shootDir = -1;
                _projectileRot = new Vector3(0, 0, 180);
            }
        }
    }

    void Shoot() 
    {
        if (_pController.fire) 
        {
            Instantiate(bullet, transform.position + new Vector3(0.35f * _shootDir, 0.5f, 0), Quaternion.Euler(_projectileRot));
        }
    }
}
