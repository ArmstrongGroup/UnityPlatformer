using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateController : MonoBehaviour
{
    enum EnemyState { active, dead, toDead };
    EnemyState _eState = EnemyState.active;

    EnemyMovement _eMove;
    CollisionChecks _cChecks;
    Rigidbody2D _rb;
    Collider2D _collider;

    bool falling;

    // Start is called before the first frame update
    void Start()
    {
        _eMove = GetComponent<EnemyMovement>();
        _cChecks = GetComponent<CollisionChecks>();
        _rb = GetComponent<Rigidbody2D>();
        _collider = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_eState)
        {
            case EnemyState.active:
                _cChecks.CollisionUpdate(); 
                _eMove.MoveUpdate();                
                FallCheck();
                break;
            case EnemyState.toDead:

                break;
            case EnemyState.dead:

                break;
            default:
                Debug.LogError("Invalid player state entered");
                break;
        }
    }

    public void Die()
    {
        _eState = EnemyState.dead;
        _collider.enabled = false;
        _rb.AddForce(transform.up * 10, ForceMode2D.Impulse);
        Destroy(gameObject, 2);
    }

    void FallCheck()
    {
        if (!_cChecks.onGround && !falling)
        {
            Debug.Log("Fall Death");
            falling = true;
            Invoke("FallDeath", 2);
        }

        if (falling && _cChecks.onGround)
        {
            Debug.Log("Cancel Fall Death");
            CancelInvoke("FallDeath");
            falling = false;
        }
    }

    void FallDeath()
    {
        Destroy(gameObject);
    }
}
