using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    PlayerStateController _pStateController;
    Rigidbody2D _rb;

    void Start()
    {
        _pStateController = GetComponent<PlayerStateController>();
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Hazard"))
        {
            KillPlayer();
        }
        else if (collision.collider.CompareTag("Enemy"))
        {
            if (collision.gameObject.transform.position.y > (transform.position.y - 0.4f))
            {
                KillPlayer();
            }
            else
            {
                _rb.velocity = Vector2.zero;
                _rb.AddForce(transform.up * 7.5f, ForceMode2D.Impulse);
                collision.gameObject.BroadcastMessage("Die");
            }
        }
    }
    void KillPlayer() 
    {
        _pStateController.PlayerDeath();
    }
}
