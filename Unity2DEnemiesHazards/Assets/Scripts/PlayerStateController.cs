using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateController : MonoBehaviour
{
    enum PlayerState {active, dead, toDead};
    PlayerState _pState = PlayerState.active;

    PlayerController _playerController;
    CharacterMovement _characterMov;
    CharacterJump _charaterJump;
    CollisionChecks _collisionChecks;
    Rigidbody2D _rb;
    Collider2D _col;

    // Start is called before the first frame update
    void Start()
    {
        _playerController = GetComponent<PlayerController>();
        _characterMov = GetComponent<CharacterMovement>();
        _charaterJump = GetComponent<CharacterJump>();
        _collisionChecks = GetComponent<CollisionChecks>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_pState)
        {
            case PlayerState.active:
                _playerController.ControlUpdate();
                _characterMov.MoveUpdate();
                _charaterJump.JumpUpdate();
                _collisionChecks.CollisionUpdate();
                break;
            case PlayerState.toDead:

                break;
            case PlayerState.dead:
                
                break;
            default:
                Debug.LogError("Invalid player state entered");
                break;
        }
    }

    public void PlayerDeath() 
    {
        _pState = PlayerState.toDead;
        _rb.simulated = false;
        _col.enabled = false;
        Invoke("DeathAnim", 1);
    }

    void DeathAnim() 
    {
        _pState = PlayerState.dead;
        _rb.simulated = true;
        _rb.velocity = Vector2.zero;
        _rb.AddForce((transform.up + new Vector3(Random.Range(-0.2f, 0.2f), 0, 0)) * 7.5f, ForceMode2D.Impulse);
    }
}
