using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public bool startMovLeft;
    public float moveSpeed = 1;
    //[Range(0.0f, 1.0f)]
    //public float acceleration = 1f;
    CollisionChecks _collisionChecks;
    Rigidbody2D _rb;

    Vector2 _velocity;
    int _dirMultiplier = 1;


    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _collisionChecks = GetComponent<CollisionChecks>();
    }

    // Start is called before the first frame update
    void Start()
    {
        if (startMovLeft)
        {
            _dirMultiplier = -1;
        }
        else 
        {
            _dirMultiplier = 1;
        }
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        Movement();
    }

    void Movement() 
    {
        _velocity = new Vector2(_velocity.x, _rb.velocity.y);
        _velocity.x = moveSpeed * (_dirMultiplier);

        //_velocity.x = Mathf.MoveTowards(_velocity.x, moveSpeed * _dirMultiplier, acceleration * Time.deltaTime * 240);

        if (_collisionChecks.leftBlock) 
        {
            _dirMultiplier = 1;
        }
        else if (_collisionChecks.rightBlock)
        {
            _dirMultiplier = -1;
        }

        _rb.velocity = _velocity;
    }
}
