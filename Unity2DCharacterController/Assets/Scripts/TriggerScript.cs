using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public string effectedTag;
    public bool inTrigger = false;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == effectedTag) 
        {
            Debug.Log("trigger endered");
            inTrigger = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == effectedTag)
        {
            Debug.Log("trigger exited");
            inTrigger = false;
        }
    }

}
