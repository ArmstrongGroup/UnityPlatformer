using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float inputX = 0;
    public bool jump;
    public bool jumpHold;

    // Start is called before the first frame update
    void Update()
    {
        GetXInput();
        GetJump();
    }

    void GetXInput()
    {
        inputX = Input.GetAxisRaw("Horizontal");
    }

    void GetJump()
    {
        jump = Input.GetButtonDown("Jump");
        jumpHold = Input.GetButton("Jump");
    }
}
