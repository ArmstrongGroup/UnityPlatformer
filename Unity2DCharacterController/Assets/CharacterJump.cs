using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJump : MonoBehaviour
{
    public float jumpForce = 5f;

    Rigidbody2D _rb;
    PlayerController _cont;
    CollisionChecks _colChecks;

    // Start is called before the first frame update
    void Start()
    {
        _cont = GetComponent<PlayerController>();
        _rb = GetComponent<Rigidbody2D>();
        _colChecks = GetComponent<CollisionChecks>();
    }

    // Update is called once per frame
    void Update()
    {
        Jump();
    }

    void Jump()
    {
        if (_cont.jump && _colChecks.onGround)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0f);
            _rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
            Debug.Log("Jump");
        }
    }

}
