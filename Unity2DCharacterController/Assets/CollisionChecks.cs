using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionChecks : MonoBehaviour
{
    public bool onGround;
    public bool leftBlock;
    public bool rightBlock;
    public LayerMask layerMask;
    public float friction;

    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EvaluateCollisions(collision);
        RetrieveFriction(collision);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        EvaluateCollisions(collision);
        RetrieveFriction(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        friction = 0;
    }

    void EvaluateCollisions(Collision2D collision)
    {
        /*onGround = false;
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector2 normal = collision.GetContact(i).normal;
            if (normal.y >= 0.9f)
            {
                onGround = true;
            }
            else
            {
                

            }
        }*/
    }

    void Update() 
    {
        GroundCheck();
        RightCheck();
        LeftCheck();
    }

    void GroundCheck() 
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 0.2f, layerMask);

        if (hit.collider != null)
        {
            //Debug.Log(hit.collider.gameObject);
            onGround = true;
            //return true;
        }
        else
        {
            //Debug.Log("Nope!");
            onGround = false;
            //return false; 
        }
    }

    void LeftCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(-0.25f,0.5f,0f), -Vector2.right, 0.2f, layerMask);

        if (hit.collider != null)
        {
            //Debug.Log(hit.collider.gameObject);
            leftBlock = true;
            //return true;
        }
        else
        {
            //Debug.Log("Nope!");
            leftBlock = false;
            //return false; 
        }
    }

    void RightCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(0.25f, 0.5f, 0f), Vector2.right, 0.2f, layerMask);

        if (hit.collider != null)
        {
            //Debug.Log(hit.collider.gameObject);
            rightBlock = true;
            //return true;
        }
        else
        {
            //Debug.Log("Nope!");
            rightBlock = false;
            //return false; 
        }
    }

    void RetrieveFriction(Collision2D collision)
    {
        PhysicsMaterial2D material = collision.collider.sharedMaterial;
        friction = 0;

        if (material != null)
        {
            friction = material.friction;
        }
    }
}
