using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateMachine : MonoBehaviour
{
    EnemyMovement _eMove;
    EnemyDeath _eDeath;

    enum EnemyState {patrolling, death, idle};
    EnemyState _eState = EnemyState.patrolling;

    // Start is called before the first frame update
    void Start()
    {
        _eMove = GetComponent<EnemyMovement>();
        _eDeath = GetComponent<EnemyDeath>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (_eState)
        {
            case EnemyState.patrolling:
                _eMove.MotionUpdate();
                break;
            case EnemyState.death:
                //_eDeath.Death();
                _eState = EnemyState.idle;
                break;
            case EnemyState.idle:

                break;

            default:
                Debug.LogError("Unassigned Enemy State");
                break;
        }
    }

    private void FixedUpdate()
    {
        switch (_eState)
        {
            case EnemyState.patrolling:
                _eMove.MotionFixedUpdate();
                break;
            case EnemyState.death:

                break;
            case EnemyState.idle:

                break;

            default:
                Debug.LogError("Unassigned Enemy State");
                break;
        }
    }

    public void Death() 
    {
        _eState = EnemyState.death;
    }
}
