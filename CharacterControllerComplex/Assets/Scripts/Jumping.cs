﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour
{
    public float jumpForce = 5f;
    public float fallMultiply = 2f;
    public float lowJumpMultiply = 4f;
    public int maxJumps = 1;

    public AnimationClip jumpAnim;
    public AnimationClip landAnim;

    float _jumpBuffCount = 0;
    float _jumpBuffTime = 0.1f;

    float _coyoteTimeCount = 0;
    float _coyoteTime = 0.2f;
    bool inAir;

    int _jumps = 0;
    Rigidbody2D _rb;
    Controller _cont;
    GroundCheck _gCheck;
    Animation _anim;

    // Start is called before the first frame update
    void Start()
    {
        _cont = GetComponent<Controller>();
        _rb = GetComponent<Rigidbody2D>();
        _gCheck = GetComponent<GroundCheck>();
        _anim = GetComponentInChildren<Animation>();
    }

    // Update is called once per frame
    public void JumpingUpdate()
    {
        CoyoteTime();
        JumpBuffer();
        ApplyForce();
        Falling();
        Landing();
    }

    void Falling() 
    {
        if (_rb.velocity.y < 0)
        {
            _rb.velocity += Vector2.up * (Physics2D.gravity.y * (fallMultiply - 1) * Time.deltaTime);
        }
        else if (_rb.velocity.y > 0 && !_cont.jumpHold) 
        {
            _rb.velocity += Vector2.up * (Physics2D.gravity.y * (lowJumpMultiply - 1) * Time.deltaTime);
        }
    }

    void ApplyForce() 
    {
        if (_jumpBuffCount > 0 && _coyoteTimeCount > 0)
        {
            _jumps = 0;
            Jump();
            _jumps++;
        }
        else if(_cont.jump && _jumps < maxJumps)
        {
            Jump();
            _jumps++;
        }
    }

    public void Jump() 
    {
        _rb.velocity = new Vector2(_rb.velocity.x, 0f);
        _rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        PlayAnimation(jumpAnim);
        inAir = true;
    }

    void CoyoteTime() 
    {
        if (_gCheck.onGround)
        {
            _coyoteTimeCount = _coyoteTime;
        }
        else 
        {
            _coyoteTimeCount -= Time.deltaTime;
        }
    }

    void JumpBuffer() 
    {
        if (_cont.jump)
        {
            _jumpBuffCount = _jumpBuffTime;
        }
        else 
        {
            _jumpBuffCount -= Time.deltaTime;
        }
    }

    void PlayAnimation(AnimationClip clip) 
    {
        _anim.Stop();
        _anim.clip = clip;
        _anim.Play();
    }

    void Landing() 
    {
        //Debug.Log(_gCheck.onGround);
        if (_gCheck.onGround && inAir)
        {
            PlayAnimation(landAnim);
            inAir = false;
        }
        else if (!_gCheck.onGround && _rb.velocity.y < 0) 
        {
            inAir = true;
        }
    }
}

