using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public float moveSpeed = 4f;
    public float pauseTime = 2f;
    public Transform[] waypoints;
    public Vector2 velocity;

    Transform _activeWaypiont;
    //Rigidbody2D _rb;
    int _activeIndex = 0;
    bool _pause = false;
    //Vector2 lastPos;
    //float lastTime;

    // Start is called before the first frame update
    void Start()
    {
        _activeWaypiont = waypoints[0];
        //_rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Motion();
        //VelUpdate();
    }

    void VelUpdate()
    {
        //float delta = Time.time - lastTime;

        //Vector2 dir = new Vector2(transform.localPosition.x, transform.localPosition.y) - lastPos;

        //dir = dir.normalized;
        //float dist = dir.magnitude;
        //float speed = dist / delta;

        //dir = dir.normalized;
        //float x = dir.x * (speed / 2);
        //float y = dir.y * (speed / 2);

        //velocity = new Vector2(x, y);
        //velocity = dir;
        //lastPos = new Vector2(transform.localPosition.x, transform.localPosition.y);
        //lastTime = Time.time;
    }

    void Motion() 
    {
        if (!_pause)
        {
            if (Vector2.Distance(transform.localPosition, _activeWaypiont.localPosition) > 0.1f)
            {
                transform.localPosition = Vector2.MoveTowards(transform.localPosition, _activeWaypiont.localPosition, moveSpeed * Time.deltaTime);
            }
            else
            {
                if (_activeIndex == waypoints.Length - 1)
                {
                    _activeWaypiont = waypoints[0];
                    _activeIndex = 0;
                }
                else
                {
                    _activeIndex++;
                    _activeWaypiont = waypoints[_activeIndex];
                }
                _pause = true;
                Invoke("UnPause", pauseTime);
            }
        }
    }

    void UnPause()
    {
        _pause = false;
    }
}
