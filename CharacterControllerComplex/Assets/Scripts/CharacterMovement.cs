﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float maxSpeed = 4f;
    public float maxAcceleration = 35f;
    public float maxAirAcceleration = 20f;

    Vector2 _desiredVelocity;
    Vector2 _velocity;

    float _maxSpeedChange;
    float _accel;

    Controller _cont;
    Rigidbody2D _rb;
    GroundCheck _gCheck;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _cont = GetComponent<Controller>();
        _gCheck = GetComponent<GroundCheck>();
    }

    public void MoveUpdate()
    {
        _desiredVelocity = new Vector2(_cont.inputX, 0f) * Mathf.Max(maxSpeed - _gCheck.friction, 0f);
    }

    // Update is called once per frame
    public void MoveFixedUpdate()
    {
        Motion();
    }

    void Motion()
    {
        //_velocity = new Vector2(_cont.inputX * 1, _rb.velocity.y);
        //_rb.velocity = _velocity;

        _velocity = _rb.velocity;

        if (_gCheck == true)
        {
            _accel = maxAcceleration;
        }
        else 
        {
            _accel = maxAirAcceleration;
        }

        _maxSpeedChange = _accel * Time.deltaTime;
        _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, _maxSpeedChange);
        _rb.velocity = _velocity;
    }
}
