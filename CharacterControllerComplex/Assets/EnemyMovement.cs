using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float maxSpeed = 4f;
    public float maxAcceleration = 35f;
    public float maxAirAcceleration = 20f;
    public Transform[] waypoints;

    Vector2 _desiredVelocity;
    Vector2 _velocity;
    Transform _activeWaypiont;

    float _maxSpeedChange;
    float _accel;
    int _dir = -1;
    int _activeIndex = 0;

    Rigidbody2D _rb;
    GroundCheck _gCheck;


    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _gCheck = GetComponent<GroundCheck>();
        _activeWaypiont = waypoints[0];
    }

    public void MotionUpdate()
    {
        if (Vector2.Distance(transform.localPosition, _activeWaypiont.localPosition) > 0.5f)
        {
            _desiredVelocity = new Vector2(_dir, 0f) * Mathf.Max(maxSpeed - _gCheck.friction, 0f);
        }
        else
        {
            if (_activeIndex == waypoints.Length - 1)
            {
                _activeWaypiont = waypoints[0];
                _activeIndex = 0;
            }
            else
            {
                _activeIndex++;
                _activeWaypiont = waypoints[_activeIndex];
            }

            if (_dir > 0) 
            {
                
            }
            _dir = _dir * -1;
            //Debug.Log(_dir);
        }
    }

    // Update is called once per frame
    public void MotionFixedUpdate()
    {
        Motion();
    }

    void Motion()
    {
        _velocity = _rb.velocity;

        if (_gCheck == true)
        {
            _accel = maxAcceleration;
        }
        else
        {
            _accel = maxAirAcceleration;
        }

        _maxSpeedChange = _accel * Time.deltaTime;
        _velocity.x = Mathf.MoveTowards(_velocity.x, _desiredVelocity.x, _maxSpeedChange);
        _rb.velocity = _velocity;
    }
}
