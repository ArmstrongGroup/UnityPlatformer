using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    Rigidbody2D _rb;
    Jumping _jump;
    GameObject _enemy;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _jump = GetComponent<Jumping>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Enemy")
        {
            KillOrDie(collision);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {

    }

    private void OnCollisionExit2D(Collision2D collision)
    {

    }

    void KillOrDie(Collision2D collision) 
    {
        bool kill = true;
        
        //float angle = collision.
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector2 pos = collision.GetContact(i).point;
            if (pos.y > transform.position.y)
            {
                kill = false;
            }
        }

        if (_rb.velocity.y < 0 && kill) 
        {
            Debug.Log("Kill");
            collision.collider.gameObject.BroadcastMessage("Death");
            _enemy = collision.gameObject;
            _jump.Jump();
        }
        else if (_enemy == null || _enemy != collision.gameObject)
        {
            _enemy = collision.gameObject;
            if (_enemy.tag == "Enemy") 
            { 
                Debug.Log(_enemy);
                Debug.Log("Player Die");
                BroadcastMessage("Died");
            }
        }
    }
}
