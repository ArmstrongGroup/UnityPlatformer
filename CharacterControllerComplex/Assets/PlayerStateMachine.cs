using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateMachine : MonoBehaviour
{
    enum PlayerState {alive, dead, toAlive, toDead};
    PlayerState _pState = PlayerState.alive;

    CharacterMovement _cMovement;
    Controller _pController;
    Jumping _pJumping;
    Collider2D[] _colliders;
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _cMovement = GetComponent<CharacterMovement>();
        _pController = GetComponent<Controller>();
        _pJumping = GetComponent<Jumping>();
        _colliders = GetComponents<Collider2D>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (_pState)
        {
            case PlayerState.alive:
                _pController.InputUpdate();
                _cMovement.MoveUpdate();
                _pJumping.JumpingUpdate();
                break;
            case PlayerState.toAlive:

                break;
            case PlayerState.dead:

                break;
            case PlayerState.toDead:

                break;

            default:
                Debug.LogError("Unassigned Player State");
                break;
        }
    }

    void FixedUpdate()
    {
        switch (_pState)
        {
            case PlayerState.alive:
                _cMovement.MoveFixedUpdate();
                break;
            case PlayerState.toAlive:

                break;
            case PlayerState.dead:

                break;
            case PlayerState.toDead:

                break;

            default:
                Debug.LogError("Unassigned Player State");
                break;
        }
    }

    public void Died() 
    {
        _pState = PlayerState.dead;
        foreach (Collider2D col in _colliders)
        {
            col.enabled = false;
        }
        _rb.velocity = Vector2.zero;
        _rb.AddForce(transform.up * 3, ForceMode2D.Impulse);
    }
}
