using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDeath : MonoBehaviour
{
    Collider2D[] _cols;
    Rigidbody2D _rb;

    // Start is called before the first frame update
    void Start()
    {
        _cols = GetComponents<Collider2D>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Death() 
    {
        foreach (Collider2D item in _cols)
        {
            item.enabled = false;
        }
        _rb.AddForce(new Vector2(Random.Range(-0.25f, 0.25f), 1) * 5,ForceMode2D.Impulse);
        Destroy(gameObject, 5);
    }
}
