using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
    public List<Image> imageSlots;
    public List<GameObject> inventorySlots;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool AddToInv(GameObject obj, Sprite pic, Color colour) 
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if (inventorySlots[i] == null) 
            {
                inventorySlots[i] = obj;
                imageSlots[i].sprite = pic;
                imageSlots[i].color = colour;
                return true;
            }
        }
        return false;
    }

    public void RemoveFromInv(GameObject obj) 
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if (inventorySlots[i] == obj)
            {
                inventorySlots[i] = null;
                inventorySlots.Capacity = 3;
                imageSlots[i].sprite = null;
                imageSlots[i].color = new Color(1,1,1,0);
                InvetoryReset();
            }
        }
    }


    void InvetoryReset() 
    {
        for (int i = 0; i < inventorySlots.Count; i++)
        {
            if (inventorySlots[i] == null)
            {
                if (i != inventorySlots.Count - 1)
                {
                    inventorySlots[i] = inventorySlots[i + 1];
                    imageSlots[i].sprite = imageSlots[i + 1].sprite;
                    imageSlots[i].color = imageSlots[i + 1].color;

                    inventorySlots[i + 1] = null;
                    imageSlots[i + 1].sprite = null;
                    imageSlots[i+1].color = new Color(1, 1, 1, 0);
                }
            }
        }
    }
}
