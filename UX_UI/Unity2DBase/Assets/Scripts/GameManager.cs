using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState {menu, inPlay, playerDied, playerDead, reset };
    public GameState _gState = GameState.inPlay;

    GameObject _player;
    PlayerStateController _pStateCont;

    List<EnemyStateController> _enemies = new List<EnemyStateController>();
    List<MovingPlatform> _movePlatforms = new List<MovingPlatform>();

    GameObject _deathUI;
    GameObject _playUI;
    

    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        _pStateCont = _player.GetComponent<PlayerStateController>();
        _deathUI = GameObject.Find("DeathUI");
        _deathUI.SetActive(false);
        _playUI = GameObject.Find("PlayUI");

        FindEnemies();
        FindPlatforms();
    }

    void FindEnemies() 
    {
        GameObject[] enems = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject item in enems)
        {
            _enemies.Add(item.GetComponent<EnemyStateController>());
        }
    }

    void FindPlatforms()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("Platform");

        foreach (GameObject item in plats)
        {
            _movePlatforms.Add(item.GetComponent<MovingPlatform>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    void StateMachine()
    {
        switch (_gState)
        {
            case GameState.inPlay:
                _pStateCont.PlayerStateUpdate();
                RunEnemies();
                RunPlatforms();
                break;

            case GameState.playerDied:
                StopEnemyMove();
                StopPlatformMove();
                _gState = GameState.playerDead;
                break;
            case GameState.playerDead:
  
                break;
            case GameState.reset:
                _deathUI.SetActive(true);
                _playUI.SetActive(false);
                _gState = GameState.menu;
                break;
            case GameState.menu:

                break;
            default:
                Debug.LogError("Invalid Game State");
                break;
        }
    }

    void RunEnemies() 
    {
        if (_enemies != null)
        {
            for (int i = 0; i < _enemies.Count; i++)
            {
                if (_enemies[i] == null)
                {
                    _enemies.Remove(_enemies[i]);
                }
                else
                {
                    _enemies[i].EnemyStateUpdate();
                }
            }
        }
    }

    void RunPlatforms() 
    {
        if (_movePlatforms != null)
        {
            for (int i = 0; i < _movePlatforms.Count; i++)
            {
                if (_movePlatforms[i] == null)
                {
                    _movePlatforms.Remove(_movePlatforms[i]);
                }
                else
                {
                    _movePlatforms[i].MovingPlatformUpdate();
                }
            }
        }
    }

    public void SetState(GameState state) 
    {
        _gState = state;
    }

    void StopPlatformMove() 
    {
        if (_movePlatforms != null)
        {
            for (int i = 0; i < _movePlatforms.Count; i++)
            {
                if (_movePlatforms[i] == null)
                {
                    _movePlatforms.Remove(_movePlatforms[i]);
                }
                else
                {
                    _movePlatforms[i].Stop();
                }
            }
        }
    }

    void StopEnemyMove()
    {
        if (_enemies != null)
        {
            for (int i = 0; i < _enemies.Count; i++)
            {
                if (_enemies[i] == null)
                {
                    _enemies.Remove(_enemies[i]);
                }
                else
                {
                    _enemies[i].Stop();
                }
            }
        }
    }
}
