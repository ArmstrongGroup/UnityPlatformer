using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float moveSpeed = 1;
    public bool moveLeftFirst;

    CollisionChecks _cChecks;
    Rigidbody2D _rb;

    Vector2 _velocity;
    int _dir = 1;

    // Start is called before the first frame update
    void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
        _cChecks = GetComponent<CollisionChecks>();
    }

    void Start()
    {
        if (moveLeftFirst)
        {
            _dir = -1;
        }
        else 
        {
            _dir = 1;
        }
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        Movement();
    }

    void Movement() 
    {
        _velocity = new Vector2(_velocity.x, _rb.velocity.y);
        _velocity.x = moveSpeed * _dir;

        if (_cChecks.leftBlock)
        {
            _dir = 1;
        }
        else if (_cChecks.rightBlock) 
        {
            _dir = -1;
        }

        _rb.velocity = _velocity;
    }
}
