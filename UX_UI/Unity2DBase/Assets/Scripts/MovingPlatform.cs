using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    public GameObject platform;
    public Transform[] waypoints;
    public float maxSpeed = 1;
    public float pauseTime;

    Transform _currentTarget;
    Vector2 _velocity;
    Rigidbody2D _rb;
    int _currentIndex = 0;

    enum PlatformState { moving, paused};
    PlatformState _platState = PlatformState.moving;


    // Start is called before the first frame update
    void Start()
    {
        _currentTarget = waypoints[0];
        _rb = platform.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    public void MovingPlatformUpdate()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_platState) 
        {
            case PlatformState.moving:
                Moving();
                break;
            case PlatformState.paused:

                break;
            default:

                break;
        }
    }

    void Moving() 
    {
        if (Vector2.Distance(platform.transform.localPosition, _currentTarget.localPosition) > 0.1f)
        {
            _velocity = Vector2.ClampMagnitude(_currentTarget.localPosition - platform.transform.localPosition, 1) * maxSpeed;
        }
        else 
        {
            if (_currentIndex == waypoints.Length - 1)
            {
                _currentIndex = 0;
                _currentTarget = waypoints[0];
            }
            else 
            {
                _currentIndex++;
                _currentTarget = waypoints[_currentIndex];
            }
            _velocity = Vector2.zero;
            _platState = PlatformState.paused;
            Invoke("UnPause", pauseTime);
        }

        _rb.velocity = _velocity;
    }

    void UnPause() 
    {
        _platState = PlatformState.moving;
    }

    public void Stop()
    {
        _rb.velocity = Vector2.zero;
    }
}
