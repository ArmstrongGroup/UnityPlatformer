using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStateController : MonoBehaviour
{
    enum EnemyState { active, dead, toDead};
    EnemyState _eState = EnemyState.active;

    EnemyMovement _eMove;
    CollisionChecks _cChecks;
    Rigidbody2D _rb;
    Collider2D _col;
    //SpriteRenderer _sRend;

    bool falling;

    // Start is called before the first frame update
    void Start()
    {
        _eMove = GetComponent<EnemyMovement>();
        _cChecks = GetComponent<CollisionChecks>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
        //_sRend = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    public void EnemyStateUpdate()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_eState) 
        {
            case EnemyState.active:

                _cChecks.CollisionUpdate();
                _eMove.MoveUpdate();
                FallCheck();

                break;
            case EnemyState.toDead:

                break;
            case EnemyState.dead:

                break;
            default:

                break;
        }
    }

    public void Die() 
    {
        _eState = EnemyState.toDead;
        _col.enabled = false;
        _rb.velocity = Vector2.zero;
        _rb.AddForce(transform.up * 5, ForceMode2D.Impulse);
        Destroy(gameObject, 2);
    }

    void FallCheck() 
    {
        if (!_cChecks.onGround && !falling) 
        {
            falling = true;
            Invoke("FallDeath", 2);
        }
        if (_cChecks.onGround && falling) 
        {
            falling = false;
            CancelInvoke("FallDeath");
        }
    }

    void FallDeath() 
    {
        Destroy(gameObject);
    }

    public void Stop()
    {
        _rb.velocity = Vector2.zero;
        _rb.simulated = false;
    }
}
