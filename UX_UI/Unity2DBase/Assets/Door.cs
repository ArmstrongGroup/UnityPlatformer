using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public GameObject door;
    public Key key;

    TriggerScript _trigger;
    Animation _anim;
    bool _doorOpen;

    // Start is called before the first frame update
    void Start()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _trigger.myTriggerEntered += MoveDoor;
        _anim = GetComponentInChildren<Animation>();
        SetDoorColourToKey();
    }

    void MoveDoor() 
    {
        if (!_doorOpen)
        {
            if (key != null)
            {
                if (key.UseKey())
                {
                    _anim.Play();
                    _doorOpen = true;
                }
            }
            else
            {
                _anim.Play();
                _doorOpen = true;
            }
        }
    }

    private void SetDoorColourToKey()
    {
        if (key != null)
        {
            door.GetComponent<SpriteRenderer>().color = key.GetComponentInChildren<SpriteRenderer>().color;
        }
    }
}
