using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public bool hasKey;
    public int numUses = 1;

    TriggerScript _trigger;
    Collider2D _col;
    SpriteRenderer _sRend;
    Color _sColour;
    Sprite _sSprite;
    InventoryController _invController;

    // Start is called before the first frame update
    void Start()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _col = GetComponentInChildren<Collider2D>();
        _sRend = GetComponentInChildren<SpriteRenderer>();
        _invController = FindObjectOfType<InventoryController>();

        _sColour = _sRend.color;
        _sSprite = _sRend.sprite;

        _trigger.myTriggerEntered += KeyPicked;
    }

    void KeyPicked() 
    {
        if (_invController.AddToInv(gameObject, _sSprite, _sColour))
        {
            _trigger.enabled = false;
            _col.enabled = false;
            _sRend.enabled = false;
            hasKey = true;
        }
    }

    public bool UseKey() 
    {
        if (hasKey) 
        {
            numUses--;
            if (numUses <= 0) 
            {
                hasKey = false;
                _invController.RemoveFromInv(gameObject);
            }
            return true;
        }
        return false;
    }
}
