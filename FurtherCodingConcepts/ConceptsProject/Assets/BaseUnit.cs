using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUnit : MonoBehaviour, iDamagable
{
    public virtual void Shout() 
    {
        Debug.Log("Whats Up?!");
    }

    public void Explode() 
    {
        Debug.Log("I exploded");
    }

    public virtual void Heal(int num) 
    {
        Debug.Log("Heal happened");
    }

    public virtual void Damage(int num)
    {
        Debug.Log("Damage of " + num + " happened");
    }
}
