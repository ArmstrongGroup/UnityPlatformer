using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComplexUnit : BaseUnit
{
    public override void Shout()
    {
        Debug.Log("Salutations");
    }

    public void Teleport() 
    {
        Debug.Log("Teleport");
    }

    public override void Heal(int num)
    {
        Debug.Log("Heal happened");
    }

    public override void Damage(int num)
    {
        Debug.Log("Better Damage of " + num*2 + " happened");
    }
}