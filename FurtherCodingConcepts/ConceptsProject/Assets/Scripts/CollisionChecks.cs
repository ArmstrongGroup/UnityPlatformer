using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionChecks : MonoBehaviour
{
    public bool onGround;
    public bool leftBlock;
    public bool rightBlock;

    public LayerMask layerMask;


    // Update is called once per frame
    void Update()
    {
        GroundCheck();
        LeftCheck();
        RightCheck();
    }

    void GroundCheck() 
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 0.2f, layerMask);

        if (hit.collider != null)
        {
            onGround = true;
        }
        else 
        {
            onGround = false;
        }
    }

    void LeftCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(-0.25f, 0.1f, 0), -Vector2.right, 0.1f, layerMask);

        if (hit.collider != null)
        {
            leftBlock = true;
        }
        else
        {
            leftBlock = false;
        }
    }

    void RightCheck()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + new Vector3(0.25f, 0.1f, 0), Vector2.right, 0.1f, layerMask);

        if (hit.collider != null)
        {
            rightBlock = true;
        }
        else
        {
            rightBlock = false;
        }
    }
}
