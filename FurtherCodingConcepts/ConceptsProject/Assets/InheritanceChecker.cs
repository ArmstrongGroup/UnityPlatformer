using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InheritanceChecker : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        BaseUnit UnitObj = collision.GetComponent<BaseUnit>();

        if (UnitObj != null)
        {
            UnitObj.Shout();
            UnitObj.Explode();

            ComplexUnit CompObj = UnitObj as ComplexUnit;

            if (CompObj != null)
            {
                CompObj.Teleport();
            }
            else 
            {
                Debug.Log("Not Complex");
            }
        }
        else
        {
            Debug.Log("Nothing to Shout");
        }
    }
}