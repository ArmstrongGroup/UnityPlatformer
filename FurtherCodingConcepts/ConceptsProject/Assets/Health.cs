using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour, iDamagable
{
    public void Heal(int num) 
    {
        Debug.Log("Heal happened");
    }

    public void Damage(int num) 
    {
        Debug.Log("Damage of " + num + " happened");
    }
}