public interface iDamagable
{
    void Heal(int num);
    void Damage(int num);
}

public interface iPickupable
{
    void AddToInventory();
    void Drop();
}