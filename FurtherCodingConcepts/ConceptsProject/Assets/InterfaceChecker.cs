using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InterfaceChecker : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        iDamagable damageObj = collision.GetComponent<iDamagable>();

        if (damageObj != null)
        {
            damageObj.Damage(5);
        }
        else 
        {
            Debug.Log("No Damage dealt");
        }
    }
}