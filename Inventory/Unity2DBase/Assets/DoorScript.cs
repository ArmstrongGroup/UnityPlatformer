using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorScript : MonoBehaviour
{
    public GameObject door;
    public KeyScript key;

    TriggerScript _trigger;
    Animation _anim;
    bool _doorOpen;

    // Start is called before the first frame update
    void Start()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _anim = GetComponentInChildren<Animation>();
        _trigger.myTriggerEntered += MoveDoor;
        SetDoorColour();
    }

    void MoveDoor() 
    {
        if (!_doorOpen) 
        {
            if (key != null)
            {
                if (key.UseKey())
                {
                    Debug.Log("Open Door");
                    _anim.Play();
                    _doorOpen = true;
                }
            }
            else 
            {
                _anim.Play();
                Debug.Log("Open Door");
                _doorOpen = true;
            }
        }
    }

    void SetDoorColour() 
    {
        if (key != null)
        {
            door.GetComponent<SpriteRenderer>().color = key.colour;
        }
    }
}
