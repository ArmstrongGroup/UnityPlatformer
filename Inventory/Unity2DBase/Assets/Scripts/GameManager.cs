using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public enum GameState {menu, inPlay, playerDied, playerDead, reset};
    public GameState _gState = GameState.inPlay;

    public List<EnemyStateController> _enemies = new List<EnemyStateController>();
    public List<MovingPlatform> _movePlatforms = new List<MovingPlatform>();

    PlayerStateController _pStateCont;

    GameObject _deathUI;
    GameObject _playUI;

    // Start is called before the first frame update
    void Start()
    {
        _pStateCont = FindObjectOfType<PlayerStateController>();
        _deathUI = GameObject.Find("DeathUI");
        _deathUI.SetActive(false);
        _playUI = GameObject.Find("PlayUI");
        

        FindEnemies();
        FindPlatforms();
    }

    void FindEnemies() 
    {
        GameObject[] enems = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject obj in enems) 
        {
            _enemies.Add(obj.GetComponent<EnemyStateController>());
        }
    }

    void FindPlatforms()
    {
        GameObject[] plats = GameObject.FindGameObjectsWithTag("MovingPlatform");
        foreach (GameObject obj in plats)
        {
            _movePlatforms.Add(obj.GetComponentInParent<MovingPlatform>());
        }
    }

    // Update is called once per frame
    void Update()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_gState) 
        {
            case GameState.inPlay:
                _pStateCont.PlayerUpdate();
                RunEnemies();
                RunPlatforms();
                break;
            case GameState.playerDied:
                StopEnemies();
                StopPlatforms();
                _gState = GameState.playerDead;
                break;
            case GameState.playerDead:

                break;
            case GameState.reset:
                _playUI.SetActive(false);
                _deathUI.SetActive(true);
                _gState = GameState.menu;
                break;
            case GameState.menu:

                break;
            default:
                Debug.LogError("Invalid Game State");
                break;
        }
        
    }

    void RunEnemies() 
    {
        if (_enemies != null) 
        {
            for (int i = 0; i < _enemies.Count; i++) 
            {
                if (_enemies[i] == null)
                {
                    _enemies.Remove(_enemies[i]);
                }
                else 
                {
                    _enemies[i].EnemyUpdate();
                }
            }
        }
    }

    void RunPlatforms()
    {
        if (_movePlatforms != null)
        {
            for (int i = 0; i < _movePlatforms.Count; i++)
            {
                if (_movePlatforms[i] == null)
                {
                    _movePlatforms.Remove(_movePlatforms[i]);
                }
                else
                {
                    _movePlatforms[i].PlatformUpdate();
                }
            }
        }
    }

    void StopEnemies() 
    {
        if (_enemies != null)
        {
            for (int i = 0; i < _enemies.Count; i++)
            {
                if (_enemies[i] == null)
                {
                    _enemies.Remove(_enemies[i]);
                }
                else
                {
                    _enemies[i].Stop();
                }
            }
        }
    }

    void StopPlatforms()
    {
        if (_movePlatforms != null)
        {
            for (int i = 0; i < _movePlatforms.Count; i++)
            {
                if (_movePlatforms[i] == null)
                {
                    _movePlatforms.Remove(_movePlatforms[i]);
                }
                else
                {
                    _movePlatforms[i].Stop();
                }
            }
        }
    }



    public void SetState(GameState state) 
    {
        _gState = state;
    }
}
