using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class DeathMenuScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ReLoadScene() 
    {
        SceneManager.LoadScene(1);
    }

    public void QuitToMenu() 
    {
        SceneManager.LoadScene(0);
    }
}
