using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float inputX = 0;
    public bool jump;
    public bool fire;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    public void InputUpdate()
    {
        GetXInput();
        GetJump();
        GetFire();
    }

    void GetXInput() 
    {
        inputX = Input.GetAxisRaw("Horizontal");
    }

    void GetJump() 
    {
        jump = Input.GetButtonDown("Jump");
    }

    void GetFire() 
    {
        fire = Input.GetButtonDown("Fire1");
    }
}
