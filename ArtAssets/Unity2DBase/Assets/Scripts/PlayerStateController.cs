using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerStateController : MonoBehaviour
{
    enum PlayerState {active, dead, toDead };
    PlayerState _pState = PlayerState.active;

    PlayerController _pControl;
    CharacterMovement _cMove;
    CharacterJump _cJump;
    CollisionChecks _cChecks;
    Rigidbody2D _rb;
    Collider2D _col;
    CinemachineVirtualCamera _vCam;
    PlayerShoot _pShoot;
    GameManager _gManager;
    AnimationController _aCont;
    
    

    // Start is called before the first frame update
    void Start()
    {
        _pControl = GetComponent<PlayerController>();
        _cMove = GetComponent<CharacterMovement>();
        _cJump = GetComponent<CharacterJump>();
        _cChecks = GetComponent<CollisionChecks>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
        _vCam = FindObjectOfType<CinemachineVirtualCamera>();
        _pShoot = GetComponent<PlayerShoot>();
        _gManager = FindObjectOfType<GameManager>();
        _aCont = GetComponent<AnimationController>();
    }

    // Update is called once per frame
    public void PlayerUpdate()
    {
        StateMachine();
    }

    void StateMachine() 
    {
        switch (_pState) 
        {
            case PlayerState.active:
                _pControl.InputUpdate();
                _cMove.MoveUpdate();
                _cJump.JumpUpdate();
                _pShoot.ShootUpdate();
                _cChecks.CollisionUpdate();
                break;
            case PlayerState.toDead:

                break;
            case PlayerState.dead:

                break;
            default:
                Debug.LogError("Invalid Player State");
                break;
        }
    }

    public void PlayerDeath() 
    {
        _pState = PlayerState.toDead;
        _rb.simulated = false;
        _col.enabled = false;
        _vCam.enabled = false;
        _gManager.SetState(GameManager.GameState.playerDied);
        _aCont.SetDeath();
        Invoke("DeathAnim", 1f);
    }

    void DeathAnim() 
    {
        _pState = PlayerState.dead;
        _rb.simulated = true;
        _rb.velocity = Vector2.zero;
        _rb.AddForce((transform.up * 7.5f), ForceMode2D.Impulse);
        Invoke("ResetGame", 2f);
    }

    void ResetGame() 
    {
        _gManager.SetState(GameManager.GameState.reset);
        _rb.velocity = Vector2.zero;
        _rb.simulated = false;
    }
}
