using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingPickup : MonoBehaviour
{
    TriggerScript _trigger;
    PlayerShoot _pShoot;
    SpriteRenderer _sRend;

    // Start is called before the first frame update
    void Awake()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _pShoot = FindObjectOfType<PlayerShoot>();
        _sRend = GetComponent<SpriteRenderer>();
    }
    // Start is called before the first frame update
    void Start()
    {
        _trigger.myTriggerEntered += PickedUp;
    }

    void PickedUp() 
    {
        _trigger.enabled = false;
        _sRend.enabled = false;
        _pShoot.hasGun = true;
    }
}
