using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterJump : MonoBehaviour
{
    public float jumpForce = 5f;

    Rigidbody2D _rb;
    PlayerController _cont;
    CollisionChecks _col;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _cont = GetComponent<PlayerController>();
        _col = GetComponent<CollisionChecks>();
    }

    // Update is called once per frame
    public void JumpUpdate()
    {
        Jump();
    }

    void Jump() 
    {
        if (_cont.jump == true && _col.onGround == true)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0f);
            _rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }
    }
}
