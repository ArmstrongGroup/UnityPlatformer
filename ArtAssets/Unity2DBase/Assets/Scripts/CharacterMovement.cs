using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    public float maxSpeed = 4f;
    [Range(0.0f, 1.0f)]
    public float acceleration = 1f;
    [Range(0.0f, 1.0f)]
    public float deceleration = 1f;

    Vector2 _desiredVelocity;
    public Vector2 velocity;

    PlayerController _cont;
    Rigidbody2D _rb;
    CollisionChecks _col;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _cont = GetComponent <PlayerController>();
        _col = GetComponent<CollisionChecks>();
    }

    // Update is called once per frame
    public void MoveUpdate()
    {
        CheckBlock();
        SetVelocity();
        _rb.velocity = velocity;
    }

    

    void CheckBlock() 
    {
        if (_col.leftBlock) 
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, 0, 1);
            velocity.x = Mathf.Clamp(velocity.x, 0, maxSpeed);
        }
        if (_col.rightBlock)
        {
            _cont.inputX = Mathf.Clamp(_cont.inputX, -1, 0);
            velocity.x = Mathf.Clamp(velocity.x, -maxSpeed, 0);
        }
    }

    void SetVelocity() 
    {
        velocity = new Vector2(velocity.x, _rb.velocity.y);

        if (_cont.inputX != 0)
        {
            _desiredVelocity = new Vector2(_cont.inputX, 0) * (maxSpeed );
            velocity.x = Mathf.MoveTowards(velocity.x, _desiredVelocity.x, acceleration * Time.deltaTime * 240);
        }
        else 
        {
            _desiredVelocity = Vector2.zero;
            velocity.x = Mathf.MoveTowards(velocity.x, _desiredVelocity.x, deceleration * Time.deltaTime * 240);
        }
    }

    private void FixedUpdate()
    {
        //_rb.velocity = _velocity;
    }

}
