using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    public float effectMultiplier = 1;

    float _startX;
    GameObject _camera;

    // Start is called before the first frame update
    void Start()
    {
        _camera = Camera.main.gameObject;
        _startX = transform.position.x;
    }

    // Update is called once per frame
    void Update()
    {
        float dist = _camera.transform.position.x * effectMultiplier;
        transform.position = new Vector3(_startX + dist, transform.position.y, transform.position.z);
    }
}
