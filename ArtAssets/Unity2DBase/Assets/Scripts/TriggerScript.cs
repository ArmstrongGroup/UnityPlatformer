using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool inTrigger = false;
    public string effectedTag;

    public delegate void TriggerEntered();
    public TriggerEntered myTriggerEntered;

    public delegate void TriggerExited();
    public TriggerExited myTriggerExited;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == effectedTag) 
        {
            inTrigger = true;
            Debug.Log("Trigger Entered");

            if (myTriggerEntered != null) 
            {
                myTriggerEntered();
            }

        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == effectedTag)
        {
            inTrigger = false;
            Debug.Log("Trigger Exited");

            if (myTriggerExited != null)
            {
                myTriggerExited();
            }
        }
    }
}
