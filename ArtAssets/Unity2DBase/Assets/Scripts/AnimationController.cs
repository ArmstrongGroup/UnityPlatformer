using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    Animator _animControl;
    SpriteRenderer _pSprite;
    PlayerController _cont;
    Rigidbody2D _rb;
    CollisionChecks _cChecks;
    CharacterMovement _cMove;

    bool inAir;

    // Start is called before the first frame update
    void Start()
    {
        _animControl = GetComponentInChildren<Animator>();
        _pSprite = GetComponentInChildren<SpriteRenderer>();
        _rb = GetComponent<Rigidbody2D>();
        _cont = GetComponent<PlayerController>();
        _cChecks = GetComponent<CollisionChecks>();
        _cMove = GetComponent<CharacterMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        SetJump();
        SetLanding();
        SetFacingDir();
    }

    void SetRunIdleAnim()
    {
        _animControl.SetFloat("RunSpeed", Mathf.Clamp(Mathf.Abs(_cMove.velocity.x),0,1.1f));
    }

    void SetFacingDir()
    {
        if (_pSprite.flipX && _cont.inputX > 0)
        {
            _pSprite.flipX = false;
        }
        else if (!_pSprite.flipX && _cont.inputX < 0)
        {
            _pSprite.flipX = true;
        }
        SetRunIdleAnim();
    }

    void SetJump() 
    {
        if (_cont.jump) 
        {
            _animControl.SetTrigger("Jumped");
            inAir = true;
        }
    }

    void SetLanding()
    {
        if (_cChecks.onGround && inAir && _rb.velocity.y < 0)
        {
            _animControl.SetTrigger("Grounded");
            inAir = false;
        }
    }

    public void SetDeath() 
    {
        _animControl.SetTrigger("Death");
    }
}
