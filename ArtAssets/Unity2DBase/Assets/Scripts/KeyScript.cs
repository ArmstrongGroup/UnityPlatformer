using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyScript : MonoBehaviour
{
    public bool hasKey;
    public int numUses = 1;
    public Color colour;

    TriggerScript _trigger;
    Collider2D _col;
    SpriteRenderer _sRend;
    Sprite _sSprite;
    Inventory _inv;


    // Start is called before the first frame update
    void Awake()
    {
        _trigger = GetComponentInChildren<TriggerScript>();
        _col = GetComponentInChildren<Collider2D>();
        _sRend = GetComponentInChildren<SpriteRenderer>();
        _inv = FindObjectOfType<Inventory>();

        colour = _sRend.color;
        _sSprite = _sRend.sprite;

        _trigger.myTriggerEntered += KeyPicked;
    }

    void KeyPicked() 
    {
        if (_inv.AddToInv(gameObject, _sSprite, colour))
        {
            _trigger.enabled = false;
            _col.enabled = false;
            _sRend.enabled = false;
            hasKey = true;
        }
    }

    public bool UseKey() 
    {
        if (hasKey) 
        {
            numUses--;
            if (numUses <= 0) 
            {
                hasKey = false;
                _inv.RemoveFromInv(gameObject);
            }
            return true;
        }
        return false;
    }
}
