﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour
{
    public float inputX = 0;
    public bool jump;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetXInput();
        GetJump();
    }

    void GetXInput() 
    {
        inputX = Input.GetAxisRaw("Horizontal");
    }

    void GetJump() 
    {
        jump = Input.GetButtonDown("Jump");
    }
}
