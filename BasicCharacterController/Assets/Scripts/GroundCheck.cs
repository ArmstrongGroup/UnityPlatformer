﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour
{
    public bool onGround;
    public float friction;

    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EvaluateCollisions(collision);
        RetrieveFriction(collision);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        EvaluateCollisions(collision);
        RetrieveFriction(collision);
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        onGround = false;
        friction = 0;
    }

    void EvaluateCollisions(Collision2D collision) 
    {
        for (int i = 0; i < collision.contactCount; i++) 
        {
            Vector2 normal = collision.GetContact(i).normal;
            if (normal.y >= 0.9f)
            {
                onGround = true;
            }
            else 
            {
                onGround = false;
            }
        }
    }

    void RetrieveFriction(Collision2D collision) 
    {
        PhysicsMaterial2D material = collision.collider.sharedMaterial;
        friction = 0;

        if (material != null) 
        {
            friction = material.friction;
        }
    }
}
