﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumping : MonoBehaviour
{
    public float jumpForce = 5f;

    Rigidbody2D _rb;
    Controller _cont;
    GroundCheck _gCheck;

    // Start is called before the first frame update
    void Start()
    {
        _cont = GetComponent<Controller>();
        _rb = GetComponent<Rigidbody2D>();
        _gCheck = GetComponent<GroundCheck>();
    }

    // Update is called once per frame
    void Update()
    {
        ApplyForce();
    }

    void ApplyForce() 
    {
        if (_cont.jump && _gCheck.onGround)
        {
            _rb.velocity = new Vector2 (_rb.velocity.x, 0f);
            _rb.AddForce(transform.up * jumpForce, ForceMode2D.Impulse);
        }
    }
}
