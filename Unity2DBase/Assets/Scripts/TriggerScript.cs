using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    public bool inTrigger = false;
    public string effectedTag;

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == effectedTag) 
        {
            inTrigger = true;
            Debug.Log("Trigger Entered");
        }
    }

    private void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag == effectedTag)
        {
            inTrigger = false;
            Debug.Log("Trigger Exited");
        }
    }
}
